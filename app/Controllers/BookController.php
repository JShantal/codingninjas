<?php

namespace App\Controllers;

use R;
use App\Resources\BookResource;
use App\Exceptions\ValidatorException;
use App\Exceptions\NotFoundHttpException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validation;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class BookController
{
    /**
     * Display a listing of the books.
     *
     * @param Request $request
     * @return \App\Resources\ResourceCollection
     */
    public function index(Request $request)
    {
        $query = 'SELECT * FROM book';
        $params = [];
        if ($limit = $this->getLimit($request)) {
            $query .= ' LIMIT ?';
            array_push($params, $limit);
        }
        if ($offset = $this->getOffset($request)) {
            $query .= ' OFFSET ?';
            array_push($params, $offset);
        }

        $books = R::getAll($query, $params);
        $books = R::convertToBeans('book', $books);
        return BookResource::collection($books);
    }

    /**
     * Store a newly created book to the storage.
     *
     * @param Request $request
     * @return BookResource
     */
    public function store(Request $request)
    {
        $request = $request->request->all();
        $this->validate($request);
        $book = R::dispense('book');
        $book->fill($request);
        R::store($book);

        return BookResource::make($book);
    }

    /**
     * Display a specified book.
     *
     * @param Request $request
     * @param int $id
     * @return BookResource
     */
    public function show(Request $request, int $id)
    {
        $book = R::load('book', $id);
        if ($book->id === 0) {
            throw new NotFoundHttpException();
        }

        return BookResource::make($book);
    }

    /**
     * Update a specified book in the storage
     *
     * @param Request $request
     * @param int $id
     * @return BookResource
     */
    public function update(Request $request, int $id)
    {
        $request = $request->request->all();
        $this->validate($request, false);
        $book = R::load('book', $id);
        if ($book->id === 0) {
            throw new NotFoundHttpException();
        }
        $book->fill($request);
        R::store($book);

        return BookResource::make($book);
    }

    /**
     * Remove a specified book from the storage
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(Request $request, int $id)
    {
        $book = R::load('book', $id);
        if ($book->id === 0) {
            throw new NotFoundHttpException();
        }
        R::trash($book);

        return new JsonResponse(null, 204);
    }

    /**
     * Get query limit from request
     *
     * @return int|bool
     */
    private function getLimit(Request $request)
    {
        $limit = $request->get('limit', 0);
        if ($limit === 'all') {
            return false;
        }
        if (!is_numeric($limit) || $limit < 1) {
            $limit = 15;
        }

        return $limit;
    }

    /**
     * Get query offset from request
     *
     * @param Request $request
     * @return int|mixed
     */
    private function getOffset(Request $request)
    {
        $offset = $request->get('offset', 0);
        if (!is_numeric($offset) || $offset < 0) {
            $offset = 0;
        }

        return $offset;
    }

    /**
     * Validate book request
     *
     * @param array $input
     * @param bool $create
     */
    private function validate(array $input, bool $create = true)
    {
        $validator = Validation::createValidator();
        $groups = new Assert\GroupSequence(['Default', 'custom']);
        $constraint = [
            'name' => [
                new Assert\Length(['min' => 1, 'max' => 191]),
                new Assert\NotBlank()
            ],
            'description' => new Assert\Optional([
                new Assert\Length(['max' => 191])
            ]),
            'published_at' => new Assert\Optional([
                new Assert\Date()
            ]),
            'authors' => new Assert\Optional([
                new Assert\Type('array')
            ])
        ];
        if (!$create) {
            $constraint['name'] = new Assert\Optional($constraint['name']);
        }
        $constraint = new Assert\Collection($constraint);

        $violations = $validator->validate($input, $constraint, $groups);
        if ($violations->count() > 0) {
            throw new ValidatorException($violations);
        }
    }
}
