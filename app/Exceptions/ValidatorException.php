<?php

namespace App\Exceptions;

use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Exception\ValidatorException as BaseValidatorException;

class ValidatorException extends BaseValidatorException
{
    public $status = 422;

    public $errors = [];

    /**
     * Create a new exception instance.
     *
     * @param ConstraintViolationList $errors
     */
    public function __construct(ConstraintViolationList $errors)
    {
        foreach ($errors as $error) {
            $field = substr($error->getPropertyPath(), 1, -1);
            $this->errors[$field][] = $error->getMessage();
        }
        parent::__construct('The given data was invalid.');
    }
}
