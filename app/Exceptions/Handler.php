<?php

namespace App\Exceptions;

use Exception;
use App\Lib\Arr;
use App\Exceptions\{
    HttpException, ValidatorException
};
use Symfony\Component\HttpFoundation\JsonResponse;

class Handler
{
    /**
     * Render an exception into a response.
     *
     * @param Exception $e
     * @return JsonResponse
     */
    public function render(Exception $e)
    {
        if ($e instanceof ValidatorException) {
            return $this->convertValidationExceptionToResponse($e);
        }

        return $this->prepareJsonResponse($e);
    }

    /**
     * Create a response object from the given validation exception.
     *
     * @param ValidatorException $e
     * @return JsonResponse
     */
    protected function convertValidationExceptionToResponse(ValidatorException $e)
    {
        return new JsonResponse([
            'message' => $e->getMessage(),
            'errors' => $e->errors,
        ], $e->status);
    }

    /**
     * Prepare a JSON response for the given exception.
     *
     * @param Exception $e
     * @return JsonResponse
     */
    protected function prepareJsonResponse(Exception $e)
    {
        $response = new JsonResponse(
            $this->convertExceptionToArray($e),
            $this->isHttpException($e) ? $e->getStatusCode() : 500,
            $this->isHttpException($e) ? $e->getHeaders() : []
        );
        $response->setEncodingOptions(JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

        return $response;
    }

    /**
     * Convert the given exception to an array.
     *
     * @param Exception $e
     * @return array
     */
    protected function convertExceptionToArray(Exception $e)
    {
        $trace = [];
        foreach ($e->getTrace() as $item) {
            $trace[] = Arr::except($item, ['args']);
        }
        return config('app.debug') ? [
            'message' => $e->getMessage(),
            'exception' => get_class($e),
            'file' => $e->getFile(),
            'line' => $e->getLine(),
            'trace' => $trace,
        ] : [
            'message' => $this->isHttpException($e) ? $e->getMessage() : 'Server Error',
        ];
    }

    /**
     * Determine if the given exception is an HTTP exception.
     *
     * @param Exception $e
     * @return bool
     */
    protected function isHttpException(Exception $e)
    {
        return $e instanceof HttpException;
    }
}
