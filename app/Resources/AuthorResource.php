<?php

namespace App\Resources;

use App\Resources\Resource;

class AuthorResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }
}
