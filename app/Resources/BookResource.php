<?php

namespace App\Resources;

use App\Resources\Resource;
use App\Resources\AuthorResource;

class BookResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'published_at' => $this->published_at,
            'authors' => AuthorResource::collection($this->sharedAuthorList)->toArray()
        ];
    }
}
