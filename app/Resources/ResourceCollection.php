<?php


namespace App\Resources;

use App\Resources\Resource;

class ResourceCollection extends Resource
{
    /**
     * The mapped collection instance.
     *
     * @var array
     */
    public $collection;

    /**
     * Create a new resource instance.
     *
     * @param $resource
     * @param $collects
     */
    public function __construct($resource, $collects)
    {
        $this->collection = [];
        if (!($resource[0] ?? null) instanceof $collects) {
            foreach ($resource as $item) {
                $this->collection[] = new $collects($item);
            }
        } else {
            $this->collection = $resource;
        }
    }

    /**
     * Transform the resource into a JSON array.
     *
     * @return array
     */
    public function toArray(): array
    {
        $array = [];
        foreach ($this->collection as $item) {
            $array[] = $item->toArray();
        }
        return $array;
    }
}
