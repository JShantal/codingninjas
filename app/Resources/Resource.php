<?php


namespace App\Resources;

use App\Resources\ResourceCollection;

abstract class Resource
{
    /**
     * The resource instance.
     *
     * @var mixed
     */
    public $resource;

    /**
     * Create a new resource instance.
     *
     * @param mixed $resource
     * @return void
     */
    public function __construct($resource)
    {
        $this->resource = $resource;
    }

    public function __get($field)
    {
        return $this->resource[$field];
    }

    /**
     * Create a new resource instance.
     *
     * @param mixed ...$parameters
     * @return static
     */
    public static function make(...$parameters)
    {
        return new static(...$parameters);
    }

    /**
     * Create a new resource collection.
     *
     * @param $resource
     * @return \App\Resources\ResourceCollection
     */
    public static function collection($resource)
    {
        return new ResourceCollection($resource, static::class);
    }

    /**
     * Transform the resource into an array.
     *
     * @return array
     */
    abstract public function toArray(): array;
}
