<?php

namespace App\Models;

use App\Models\Model;

class Author extends Model
{
    protected $fillable = [
        'name'
    ];
}
