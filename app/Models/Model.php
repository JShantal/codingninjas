<?php


namespace App\Models;

use App\Lib\Arr;
use RedBeanPHP\SimpleModel;

class Model extends SimpleModel
{
    protected $fillable = [];

    /**
     * Fill model fillable fields with array
     *
     * @param array $data
     */
    public function fill(array $data)
    {
        $data = Arr::only($data, $this->fillable);
        foreach ($data as $field => $value) {
            $this->bean->$field = $value;
        }
    }
}
