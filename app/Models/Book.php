<?php

namespace App\Models;

use R;
use App\Lib\Arr;
use App\Models\Model;

class Book extends Model
{
    protected $fillable = [
        'name',
        'description',
        'published_at'
    ];

    /**
     * Fill model fillable fields with array
     *
     * @param array $data
     */
    public function fill(array $data)
    {
        parent::fill($data);
        if (!empty($authors = Arr::get($data, 'authors'))) {
            $this->setAuthors($authors);
        }
    }

    /**
     * Set book authors
     *
     * @param array $authors
     */
    public function setAuthors(array $authors)
    {
        $stored = R::find('author', ' name IN (' . R::genSlots($authors) . ')', $authors);
        $stored_names = Arr::pluck($stored, 'name');
        $unstored = array_udiff($authors, $stored_names, 'strcasecmp');
        $unstored = $this->storeAuthors($unstored);
        $authors = array_merge($stored, $unstored);
        $this->bean->sharedAuthorList = $authors;
    }

    /**
     * Store newly created authors in the storage
     *
     * @param array $names
     * @return array
     */
    private function storeAuthors(array $names): array
    {
        $authors = [];
        foreach ($names as $name) {
            $author = R::dispense('author');
            $author->name = $name;
            R::store($author);
            $authors[] = $author;
        }

        return $authors;
    }
}
