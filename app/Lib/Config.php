<?php

namespace App\Lib;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Exception\FileLocatorFileNotFoundException;

class Config
{
    /**
     * Get data from config file
     *
     * @param string $path
     * @param mixed $default
     * @return mixed
     */
    public static function get(string $path, $default = null)
    {
        if (empty($path)) {
            return value($default);
        }
        $path = explode('.', $path, 2);
        $filename = $path[0];
        $path = $path[1] ?? null;
        try {
            $config = self::load($filename);
        } catch (FileLocatorFileNotFoundException $e) {
            return value($default);
        }

        return Arr::get($config, $path, $default);
    }

    /**
     * Load config file
     *
     * @param string $filename
     * @return array|null
     */
    private static function load(string $filename): ?array
    {
        $locator = new FileLocator(config_path());
        $file = $locator->locate("$filename.yml");
        return Yaml::parseFile($file);
    }
}
