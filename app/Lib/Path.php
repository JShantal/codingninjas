<?php

namespace App\Lib;

class Path
{
    /**
     * Get the base path of the application.
     *
     * @param string $path
     * @return string
     */
    public static function basePath(string $path = ''): string
    {
        $path = self::getRootPath() . ($path ? DIRECTORY_SEPARATOR . $path : $path);

        return realpath($path);
    }

    /**
     * Get the config path of the application.
     *
     * @param string $path
     * @return string
     */
    public static function configPath(string $path = ''): string
    {
        return self::basePath('config') . ($path ? DIRECTORY_SEPARATOR . $path : $path);
    }

    /**
     * Get the database path of the application.
     *
     * @param string $path
     * @return string
     */
    public static function databasePath(string $path = ''): string
    {
        return self::basePath('database') . ($path ? DIRECTORY_SEPARATOR . $path : $path);
    }

    /**
     * Get the application root.
     *
     * @return string
     */
    private static function getRootPath()
    {
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..';
    }
}
