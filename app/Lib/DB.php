<?php

namespace App\Lib;

use R;
use App\Lib\Arr;
use InvalidArgumentException;

class DB
{
    /**
     * Setup database connection
     */
    public static function setup()
    {
        define('REDBEAN_MODEL_PREFIX', '\\App\\Models\\');
        $config = self::extractRConfig();
        R::setup(...$config);
        R::freeze();
    }

    /**
     * Close database connection
     */
    public static function close()
    {
        R::close();
    }

    /**
     * Extract config for RedBean
     *
     * @return array
     */
    private static function extractRConfig(): array
    {
        $connection = self::extractConnectionConfig();
        $driver = Arr::get($connection, 'driver');
        switch ($driver) {
            case 'mysql':
                return self::extractMySQLConnection($connection);
                break;
            case 'pgsql':
                return self::extractPgSQLConnection($connection);
                break;
            case 'sqlite':
                return self::extractSQLiteConnection($connection);
                break;
            default:
                throw new InvalidArgumentException("Unsupported driver [{$driver}]");
        }
    }

    /**
     * Extract default connection config
     *
     * @return array
     */
    private static function extractConnectionConfig(): array
    {
        $config = config('database');
        $name = Arr::get($config, 'default');
        return Arr::get($config, "connections.$name", function () use ($name) {
            throw new InvalidArgumentException("Database [{$name}] not configured.");
        });
    }

    /**
     * Extract MySQL connection
     *
     * @param array $connection
     * @return array
     */
    private static function extractMySQLConnection(array $connection): array
    {
        list($host, $port, $dbname, $user, $password) = self::extractConnection($connection);
        return ["mysql:host=$host;port=$port;dbname=$dbname", $user, $password];
    }

    /**
     * Extract PgSQL connection
     *
     * @param array $connection
     * @return array
     */
    private static function extractPgSQLConnection(array $connection): array
    {
        list($host, $port, $dbname, $user, $password) = self::extractConnection($connection);
        return ["pgsql:host=$host;port=$port;dbname=$dbname", $user, $password];
    }

    /**
     * Extract SQLite connection
     *
     * @param array $connection
     * @return array
     */
    private static function extractSQLiteConnection(array $connection): array
    {
        $path = Arr::get($connection, 'path', 'database.sqlite');
        if (!Str::startsWith($path, DIRECTORY_SEPARATOR)) {
            $path = database_path($path);
        }
        return ["sqlite:$path"];
    }

    /**
     * Extract database connection with default values
     *
     * @param array $connection
     * @return array
     */
    private static function extractConnection(array $connection): array
    {
        if (Arr::get($connection, 'driver') === 'pgsql') {
            $defport = 5342;
        } else {
            $defport = 3306;
        }
        return [
            Arr::get($connection, 'host', 'localhost'),
            Arr::get($connection, 'port', $defport),
            Arr::get($connection, 'dbname', 'forge'),
            Arr::get($connection, 'user', 'forge'),
            Arr::get($connection, 'password')
        ];
    }
}
