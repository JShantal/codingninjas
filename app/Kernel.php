<?php

namespace App;

use App\Lib\DB;
use App\Resources\Resource;
use App\Exceptions\Handler;
use App\Exceptions\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class Kernel
{
    /**
     * Handle Request to convert it to a Response.
     *
     * @param Request $request
     * @return JsonResponse|Response
     */
    public static function handle(Request $request)
    {
        self::registerAliases();
        DB::setup();
        try {
            $content = self::sendRequestThroughRouter($request);
        } catch (\Exception $e) {
            DB::close();
            $handler = new Handler();
            return $handler->render($e);
        }
        DB::close();
        return self::wrapContentWithResponse($content);
    }

    /**
     * Register application aliases
     */
    private static function registerAliases()
    {
        $aliases = config('app.aliases', []);
        foreach ($aliases as $alias => $class) {
            class_alias($class, $alias);
        }
    }

    /**
     * Send Request through Router.
     *
     * @param Request $request
     * @return JsonResponse|Response
     */
    private static function sendRequestThroughRouter(Request $request)
    {
        $routes = self::getRoutes();
        $context = new Routing\RequestContext();
        $context->fromRequest($request);
        $matcher = new Routing\Matcher\UrlMatcher($routes, $context);
        try {
            $attributes = $matcher->matchRequest($request);
            $action = 'App\Controllers\\' . $attributes['action'];
            $attributes = self::prepareControllerAttributes($attributes, $request);
            list($class, $action) = explode('@', $action);
            $content = call_user_func_array([new $class(), $action], $attributes);
        } catch (ResourceNotFoundException $e) {
            throw new NotFoundHttpException();
        }

        return $content;
    }

    /**
     * Get route list from file
     *
     * @return Routing\RouteCollection
     */
    private static function getRoutes()
    {
        $locator = new FileLocator(__DIR__ . '/../config');
        $loader = new YamlFileLoader($locator);
        return $loader->load('routes.yml');
    }

    /**
     * Prepare controller action attributes
     *
     * @param array $attributes
     * @param Request $request
     * @return array
     */
    private static function prepareControllerAttributes(array $attributes, Request $request): array
    {
        unset($attributes['action']);
        array_unshift($attributes, $request);

        return $attributes;
    }

    /**
     * Wrap content to Response object
     *
     * @param $content
     * @return JsonResponse|Response
     */
    private static function wrapContentWithResponse($content)
    {
        if ($content instanceof Response) {
            return $content;
        }
        if (is_null($content)) {
            return new Response();
        }
        if ($content instanceof Resource) {
            $content = $content->toArray();
        }
        return new JsonResponse($content);
    }
}
