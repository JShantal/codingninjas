### CodingNinjas Test Task (PHP REST API) by Ivan Slesarenko

# Configuration

## Public Directory

You should configure your web server's document / web root to be the  public directory. The index.php in this directory serves as the front controller for all HTTP requests entering your application.

## Configuration Files

All of the configuration files for the application are stored in the config directory.

## Database 

Config your database connection in the `/config/database.yml` file. `MySQL`, `PgSQL` and `SQLite` drivers are available.

Sql file for creating database structure is located in `database/mysql_dump.sql` 

# Routes

## `GET /`

Returns array of books with authors.

### Params: 

- Limit: set the quantity of books to be returned 
- Offset: set the quantity of books to skip 

### Response
``` json
[
    {
        "id": "1",
        "name": "In Search of Lost Time",
        "description": "Lorem ipsum dolor",
        "published_at": 2012-06-23,
        "authors": [
            {
                "id": "1",
                "name": "Marcel Proust"
            },
            {
                "id": "4",
                "name": "Miguel de Cervantes"
            },
        ]
    },
    {
        "id": "2",
        "name": "Don Quixote",
        "description": "Lorem ipsum dolor",
        "published_at": "1880-04-02",
        "authors": [
            {
                "id": "4",
                "name": "Miguel de Cervantes"
            }
        ]
    },
]
```

## `POST /`

Stores a book to the database.

Returns newly created object.

### Fields: 

- `name`: max length 191, requried, unique.
- `description`: max length 191.
- `published_at`: date, format `Y-m-d`.
- `authors`: array of authors names. Max length 191 for each name.

### Response

``` json
{
    "id": "1",
    "name": "In Search of Lost Time",
    "description": "Lorem ipsum dolor",
    "published_at": 2012-06-23,
    "authors": [
        {
            "id": "1",
            "name": "Marcel Proust"
        },
        {
            "id": "4",
            "name": "Miguel de Cervantes"
        },
    ]
}
```

## `Show /{id}`

Returns a book with specified id from database.

### Response

``` json
{
    "id": "1",
    "name": "In Search of Lost Time",
    "description": "Lorem ipsum dolor",
    "published_at": 2012-06-23,
    "authors": [
        {
            "id": "1",
            "name": "Marcel Proust"
        },
        {
            "id": "4",
            "name": "Miguel de Cervantes"
        },
    ]
}
```

## `PATCH /{id}`, `PUT /{id}`

Updates a book in the database using specified id as a key.

Returns updated object.

### Fields: 

- `name`: max length 191, unique.
- `description`: max length 191.
- `published_at`: date, format `Y-m-d`.
- `authors`: array of authors names. Max length 191 for each name.

If authors field is not preset - the old field value will be used. Else author list will be replaced with new values.

### Response

``` json
{
    "id": "1",
    "name": "In Search of Lost Time",
    "description": "Cool description",
    "published_at": 2012-06-23,
    "authors": [
        {
            "id": "1",
            "name": "Marcel Proust"
        }
    ]
}
```

## `DELETE /{id}`

Deletes a book from the database using specified id as a key. No response.
